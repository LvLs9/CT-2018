#include <iostream>

using namespace std;

const int N = 10000005;
const int A = 65535;
const int B = 1073741823;

int n, x, y, m, z, t;
int a[N];
long long a_sum[N], ans = 0;
int b[N * 2];

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen("sum0.in", "r", stdin);
    freopen("sum0.out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    cin >> n >> x >> y >> a[0]
        >> m >> z >> t >> b[0];

    for (int i = 0; i < n; ++i) {
        a[i + 1] = (x * a[i] + y) & A;
        a_sum[i + 1] = a_sum[i] + a[i];
    }

    for (int i = 0; i < m * 2; ++i) {
        b[i + 1] = (z * b[i] + t) & B;
    }

    for (int i = 0; i < m * 2; i += 2) {
        b[i] %= n;
        b[i + 1] %= n;
        if (b[i] < b[i + 1]) {
            swap(b[i], b[i + 1]);
        }
    }

    for (int i = 0; i < m * 2; i += 2) {
        ans += a_sum[b[i] + 1] - a_sum[b[i + 1]];
    }

    cout << ans << '\n';
    return 0;
}