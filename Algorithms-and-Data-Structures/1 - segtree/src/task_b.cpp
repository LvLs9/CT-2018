#include <bits/stdc++.h>

#define TASK "rsq"

using namespace std;

const int N = static_cast<const int>(5e5 + 5);

int n;
long long a[N];
long long t[N * 4];

long long build(int v = 0, int begin = 0, int end = n) {
    if (end - begin == 1) {
        return t[v] = a[begin];
    } else {
        int mid = (begin + end) / 2;
        return t[v] = build(v * 2 + 1, begin, mid) + build(v * 2 + 2, mid, end);;
    }
}

long long getSum(int from, int to, int v = 0, int begin = 0, int end = n) {
    if (from >= to) {
        return 0;
    } else if (from == begin && to == end) {
        return t[v];
    } else {
        int mid = (begin + end) / 2;
        return getSum(from, min(to, mid), v * 2 + 1, begin, mid) + getSum(max(from, mid), to, v * 2 + 2, mid, end);
    }
}

long long setElement(int to, long long value, int v = 0, int begin = 0, int end = n) {
    if (end - begin == 1) {
        return t[v] = a[to] = value;
    } else {
        int mid = (begin + end) / 2;
        if (to < mid) {
            return t[v] = setElement(to, value, v * 2 + 1, begin, mid) + t[v * 2 + 2];
        } else {
            return t[v] = t[v * 2 + 1] + setElement(to, value, v * 2 + 2, mid, end);
        }
    }
}

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> a[i];
    }
    build();

    string cmd;
    int i, j;
    while (cin >> cmd >> i >> j) {
        if (cmd == "sum") {
            printf("%lli\n", getSum(i - 1, j));
        }
        if (cmd == "set") {
            setElement(i - 1, j);
        }
    }

    return 0;
}