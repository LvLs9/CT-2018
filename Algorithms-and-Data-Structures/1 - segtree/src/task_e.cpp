#include <iostream>

#define TASK "crypto"

using namespace std;

const int N = static_cast<const int>(2e5 + 5);

int r, n, m;

struct Matrix {
    long long a = 1, b = 0;
    long long c = 0, d = 1;

    void getFromCin() {
        cin >> a >> b >> c >> d;
    }

    void printToCout() {
        cout << a << ' ' << b << '\n' << c << ' ' << d << "\n\n";
    }
} a[N], neutral;

Matrix operator*(Matrix a, Matrix b) {
    Matrix ans;
    ans.a = ((a.a * b.a) + (a.b * b.c)) % r;
    ans.b = ((a.a * b.b) + (a.b * b.d)) % r;
    ans.c = ((a.c * b.a) + (a.d * b.c)) % r;
    ans.d = ((a.c * b.b) + (a.d * b.d)) % r;
    return ans;
}

struct SegmentTree {
    Matrix data;
    int begin;
    int end;
    SegmentTree *left = nullptr;
    SegmentTree *right = nullptr;

    SegmentTree(int begin, int end) {
        this->begin = begin;
        this->end = end;
    }

    Matrix build() {
        if (end - begin == 1) {
            data = a[begin];
        } else {
            int mid = (begin + end) / 2;
            data = (left = new SegmentTree(begin, mid))->build() * (right = new SegmentTree(mid, end))->build();
        }
#if AT_LVLS_PC
        fprintf(stderr, "FROM %d TO %d\n%d %d\n%d %d\n\n", begin, end, data.a, data.b, data.c, data.d);
#endif
        return data;
    }

    Matrix get(int from, int to) {
        if (from >= to) {
            return neutral;
        } else if (from == begin && to == end) {
            return data;
        } else {
            int mid = (begin + end) / 2;
            return left->get(from, min(to, mid)) * right->get(max(from, mid), to);
        }
    }
};

int main() {
#if ON_MY_PC
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#else
    freopen(TASK".in", "r", stdin);
    freopen(TASK".out", "w", stdout);
#endif
    ios::sync_with_stdio(false);

    cin >> r >> n >> m;
    SegmentTree tree(0, n);
    for (int i = 0; i < n; ++i) {
        a[i].getFromCin();
    }
    tree.build();
    for (int l, r, i = 0; i < m; ++i) {
        cin >> l >> r;
        tree.get(l - 1, r).printToCout();
    }

    return 0;
}